from django.contrib import admin

# Register your models here.

from .models import Question
from .models import Choice
from .models import Answer


class QuestionAdmin(admin.ModelAdmin):
    search_fields = ['question_text']
    list_filter = ['pub_date', 'question_type']
    list_display = ['question_text']

class ChoiceAdmin(admin.ModelAdmin):
    search_fields = ['choice_text']
    list_filter = ['question', 'votes']
    list_display = ['question','choice_text']

class AnswerAdmin(admin.ModelAdmin):
    search_fields = ['answer_text']
    list_display = ['answer_text']


admin.site.register(Question, QuestionAdmin)
admin.site.register(Choice, ChoiceAdmin)
admin.site.register(Answer, AnswerAdmin)
